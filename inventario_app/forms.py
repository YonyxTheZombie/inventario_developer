from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.utils import ValidationError
from django.shortcuts import get_object_or_404, redirect, render
from django.forms.widgets import RadioSelect, Textarea, DateInput, DateTimeBaseInput, DateTimeInput
from django.forms.models import inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, HTML, ButtonHolder, Submit
from inventario_app.models import (User, Inventario, Marca, Equipo, Colegios, Administrador, Soporte, Laboratorio, Adicional, Monitor, Incidencia, 
                                                Reporte, Extras, Despacho, ReporteNew, Insumos, Insumoform, Insumo_Egreso)



class AdministradorSignUpForm(UserCreationForm):
        
    colegio = forms.ModelMultipleChoiceField(
        queryset=Colegios.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )
    email = forms.EmailField(label="Correo Institucional", required=True, help_text="Debe ingresar el correo dado por la fundación o el colegio.")
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)
    

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("El correo ya está registrado.")
        return email
 
   
    class Meta:
        model = User
        fields = [
                'username',
                'first_name',
                'last_name',
                'email',
                'password1',
                'password2'
                
        ]
        labels = {
            'username': 'Nombre de usuario',
            'first_name':'Nombre',
            'last_name':'Apellido',
        }   
    
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_administrador = True
        user.save()
        administrador = Administrador.objects.create(user=user)
        administrador.colegio.add(*self.cleaned_data.get('colegio'))
        administrador.email = self.cleaned_data["email"]
        administrador.password1 = self.cleaned_data["password1"]
        
        return user



class SoporteSignUpForm(UserCreationForm):
        
    colegio = forms.ModelMultipleChoiceField(
        queryset=Colegios.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )
    email = forms.EmailField(label="Correo Institucional", required=True, help_text="Debe ingresar el correo dado por la fundación o el colegio.")
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)
    

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("El correo ya está registrado.")
        return email
 
   
    class Meta:
        model = User
        fields = [
                'username',
                'first_name',
                'last_name',
                'email',
                'password1',
                'password2'
                
        ]
        labels = {
            'username': 'Nombre de usuario',
            'first_name':'Nombre',
            'last_name':'Apellido',
        }   
    
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_soporte = True
        user.save()
        soporte = Soporte.objects.create(user=user)
        soporte.colegio.add(*self.cleaned_data.get('colegio'))
        soporte.email = self.cleaned_data["email"]
        soporte.password1 = self.cleaned_data["password1"]
        
        return user

class LaboratorioSignUpForm(UserCreationForm):
        
    colegio = forms.ModelMultipleChoiceField(
        queryset=Colegios.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )
    email = forms.EmailField(label="Correo Institucional", required=True, help_text="Debe ingresar el correo dado por la fundación o el colegio.")
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)
    

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("El correo ya está registrado.")
        if not "@bostoneduca" in email:
            raise ValidationError("Ingrese un correo institucional.")
        return email
 
   
    class Meta:
        model = User
        fields = [
                'username',
                'first_name',
                'last_name',
                'email',
                'password1',
                'password2'
                
        ]
        labels = {
            'username': 'Nombre de usuario',
            'first_name':'Nombre',
            'last_name':'Apellido',
        }   
    
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_laboratorio = True
        user.save()
        laboratorio = Laboratorio.objects.create(user=user)
        laboratorio.colegio.add(*self.cleaned_data.get('colegio'))
        laboratorio.email = self.cleaned_data["email"]
        laboratorio.password1 = self.cleaned_data["password1"]
        
        return user


class CrearInventarioForm(forms.ModelForm):
    
    
    class Meta:
        model= Inventario
        fields = ('colegio','codigo','equipo','marca','modelo','serie','numero_doc','Fecha','ubicacion','usuario','condicion','valido','imagen')
        widgets = {
        'Fecha': forms.DateInput(attrs={'class':'form-control', 'placeholder':'selecciona una fecha', 'type':'date'}),
        }
    


    def clean_codigo(self):
        codigo = self.cleaned_data['codigo'].lower()
        colegio = self.cleaned_data['colegio']
        r = Inventario.objects.filter(codigo=codigo, colegio=colegio)
        if r:
            raise  ValidationError("El codigo ya está registrado.")
        return codigo

class EditarInventarioForm(forms.ModelForm):
    
    
    class Meta:
        model= Inventario
        fields = ('colegio','codigo','equipo','marca','modelo','serie','numero_doc','Fecha','ubicacion','usuario','condicion','valido','imagen')
        widgets = {
        'Fecha': forms.TextInput(attrs={'class':'form-control', 'placeholder':'selecciona una fecha', 'type':'date'}),
        }

class CrearCaracteristicasForm(forms.ModelForm):

    class Meta:
        model = Adicional
        fields = ('procesador','ram','sistema','disco','disco','placa','office')


class CrearMonitorForm(forms.ModelForm):
    class Meta:
        model = Monitor
        fields = ('marca','modelo','serie','imagen')

class IncidenciaForm(forms.ModelForm):
    class Meta: 
        model = Incidencia
        fields = ('texto',)

class ReporteForm(forms.ModelForm):
    class Meta: 
        model = Reporte
        fields = ('colegio','texto',)


class ReporteNewForm(forms.ModelForm):
    class Meta:
        model = ReporteNew
        fields = ('texto',)

class DespachoForm(forms.ModelForm):
    extras = forms.ModelMultipleChoiceField(
        queryset=Extras.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False
    )

    class Meta:
        model = Despacho
        fields = ('usuario','area','solicitado','fecha_entrega','destino','extras')
        widgets = {
        'fecha_entrega': forms.DateInput(attrs={'class':'form-control', 'placeholder':'selecciona una fecha', 'type':'date'}),
        }
    
class InsumoForm(forms.ModelForm):
    class Meta:
        model = Insumoform
        fields = ('insumo','colegio','factura','cantidad','fecha','proveedor')
        widgets = {
        'fecha': forms.DateInput(attrs={'class':'form-control', 'placeholder':'selecciona una fecha', 'type':'date'}),
        }