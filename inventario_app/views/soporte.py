import json
import traceback
import calendar
from datetime import date, datetime, time, timedelta
from itertools import chain
from .__init__ import *
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count, Sum, Min, Max, Func
from django.contrib.auth.models import User
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect, HttpRequest
from django.views.generic import (TemplateView,CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from ..decorators import soporte_required
                                  
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from rest_framework.views import APIView
from rest_framework.response import Response
from inventario_app.forms import SoporteSignUpForm, CrearInventarioForm, CrearCaracteristicasForm, CrearMonitorForm, IncidenciaForm, ReporteForm, ReporteNewForm
from inventario_app.models import Inventario, Marca, User, Equipo, Administrador, Colegios, Adicional, Monitor, Incidencia, Reporte, ReporteNew
# Create your views here.

class SoporteSignUpView(CreateView):
    model = User
    form_class = SoporteSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'Soporte'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('soporte:index')

@login_required
@soporte_required
def index(request):
    soporte = request.user.soporte
    colegio_soporte = soporte.colegio.values_list('pk', flat=True)
    inventario = Inventario.objects.filter(colegio__in=colegio_soporte).select_related('colegio')

    return render(request, "inventario_app/Soporte/index.html",{'inventario':inventario})


@method_decorator([login_required, soporte_required], name='dispatch')
class CrearInventarioView(CreateView):
    model = Inventario
    form_class = CrearInventarioForm
    template_name='inventario_app/soporte/crearinventario.html'

    def form_valid(self, form):
        inventario = form.save(commit=False)
        inventario.user = self.request.user
        inventario.save()
        messages.success(self.request, 'inventario agregado')
        return redirect('soporte:inventario_detalle', inventario.pk)



@login_required
@soporte_required
def crear_adicional(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = CrearCaracteristicasForm(request.POST,  request.FILES)
        if form.is_valid():
            adicional = form.save(commit=False)
            adicional.inventario = inventario
            adicional.save()
            messages.success(request, 'adicional creado')
            return redirect('soporte:inventario_detalle', inventario.pk)
    else:
        form = CrearCaracteristicasForm()
    return render(request, 'inventario_app/soporte/crear_adicional.html',{'inventario':inventario,'form':form})



@login_required
@soporte_required
def crear_monitor(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = CrearMonitorForm(request.POST,  request.FILES)
        if form.is_valid():
            monitor = form.save(commit=False)
            monitor.inventario = inventario
            monitor.save()
            messages.success(request, 'monitor creado')
            return redirect('soporte:inventario_detalle', inventario.pk)
    else:
        form = CrearMonitorForm()
    return render(request, 'inventario_app/soporte/crear_monitor.html',{'inventario':inventario,'form':form})



@login_required
@soporte_required
def crear_incidencia(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = IncidenciaForm(request.POST,  request.FILES)
        if form.is_valid():
            incidencia = form.save(commit=False)
            incidencia.inventario = inventario
            incidencia.save()
            messages.success(request, 'Incidencia creada')
            return redirect('soporte:inventario_detalle', inventario.pk)
    else:
        form = IncidenciaForm()
    return render(request, 'inventario_app/soporte/crear_incidencia.html',{'inventario':inventario,'form':form})

@login_required
@soporte_required
def crear_reporte(request):

    if request.method == 'POST':
        form = ReporteForm(request.POST,  request.FILES)
        if form.is_valid():
            reporte = form.save(commit=False)
            reporte.user = request.user
            reporte.save()
            messages.success(request, 'Reporte creado')
            return redirect('soporte:reportes')
    else:
        form = ReporteForm()
    return render(request, 'inventario_app/soporte/crear_reporte.html',{'form':form})

@login_required
@soporte_required
def crear_reporte_new(request, pk):
    reporte = get_object_or_404(Reporte, pk=pk)
    if request.method == 'POST':
        form = ReporteNewForm(request.POST,  request.FILES)
        if form.is_valid():
            reportenew = form.save(commit=False)
            reportenew.reporte = reporte
            reportenew.user = request.user
            reportenew.save()
            messages.success(request, 'Reporte creado')
            return redirect('soporte:reportes_view', reporte.pk)
    else:
        form = ReporteNewForm()
    return render(request, 'inventario_app/soporte/crear_reporte_new.html',{ 'reporte':reporte, 'form':form})
    
@login_required
@soporte_required
def descripcion(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)
    adicional = Adicional.objects.filter(inventario=inventario)
    monitor = Monitor.objects.filter(inventario=inventario)
    incidencia = Incidencia.objects.filter(inventario=inventario)
    return render(request, "inventario_app/soporte/descripcion.html", {'inventario':inventario, 'adicional':adicional,'monitor':monitor,'incidencia':incidencia })

@login_required
@soporte_required
def reportes(request):
    reporte = Reporte.objects.filter(user=request.user)
    date = datetime.now()
    hora = date.strftime("%H:%M:%S")
    return render(request, "inventario_app/soporte/reportes.html", {'reporte':reporte, 'hora':hora })

@login_required
@soporte_required
def reportes_view(request, pk):
    reporte = get_object_or_404(Reporte, pk=pk)
    reporte_new = ReporteNew.objects.filter(reporte=reporte).order_by('-tiempo')

    return render(request, "inventario_app/soporte/reporte_view.html", {'reporte':reporte, 'reporte_new':reporte_new})